CC = g++
LD = g++
override CCFLAGS+= -Wall -std=gnu++11
LDFLAGS =-Wall

DIR_SRC = src
DIR_OBJ = obj
DIR_BIN = bin
DIR_LIB = lib
DIR_HDR = include
DIR_RES = res
DIR_STATIC = static
DIR_SHARED = shared

ifeq ($(OS),Windows_NT)
  WIN=1
  RM =del /s /f /q
  COLOR_BLUE =
  COLOR_GREEN=
  COLOR_RED  =
  COLOR_DEF  =
else
  WIN=0
  RM =rm -rf
  COLOR_BLUE =\033[1;34m
  COLOR_GREEN=\033[1;32m
  COLOR_RED  =\033[31m
  COLOR_DEF  =\033[0m
endif

DIR_OBJ_STATIC = $(DIR_OBJ)/$(DIR_STATIC)
DIR_OBJ_SHARED = $(DIR_OBJ)/$(DIR_SHARED)
DIR_LIB_STATIC = $(DIR_LIB)/$(DIR_STATIC)
DIR_LIB_SHARED = $(DIR_LIB)/$(DIR_SHARED)

DIRS = $(DIR_OBJ) $(DIR_LIB)
DIRS_SHARED = $(DIR_OBJ_SHARED) $(DIR_LIB_SHARED)
DIRS_STATIC = $(DIR_OBJ_STATIC) $(DIR_LIB_STATIC)

PJ_SRC_WPATH = $(addprefix $(DIR_SRC)/,$(PROJECT_SRC))
PJ_OBJ_STATIC_WPATH = $(addprefix $(DIR_OBJ_STATIC)/,$(patsubst %.cpp,%.o,$(PROJECT_SRC)))
PJ_OBJ_SHARED_WPATH = $(addprefix $(DIR_OBJ_SHARED)/,$(patsubst %.cpp,%.o,$(PROJECT_SRC)))
PJ_LIB_STATIC_WPATH = $(DIR_LIB_STATIC)/lib$(PROJECT_NAME).a
ifeq ($(WIN),0)
  PJ_LIB_SHARED_WPATH = $(DIR_LIB_SHARED)/lib$(PROJECT_NAME).so
  CCFLAGS_SHARED = $(CCFLAGS) -I$(DIR_HDR) -D_DIG_LOGGER_COMPILE_TIME_ -fpic
  LDFLAGS_SHARED = $(LDFLAGS) -shared
else
  PJ_LIB_SHARED_WPATH = $(DIR_LIB_SHARED)/$(PROJECT_NAME).dll
  CCFLAGS_SHARED = $(CCFLAGS) -I$(DIR_HDR) -D_DIG_LOGGER_COMPILE_TIME_
  LDFLAGS_SHARED = $(LDFLAGS) -shared -Wl,--out-implib,$(DIR_LIB_SHARED)/lib$(PROJECT_NAME).a
endif

CCFLAGS_STATIC = $(CCFLAGS) -I$(DIR_HDR) -DDIG_LOGGER_STATIC -O3

LDFLAGS_STATIC = rcs